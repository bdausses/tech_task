# Quick Docker Proxy Example
This repo contains demo code to illustrate using 2 NGINX Docker containers together to serve/proxy sensitive content.  A web container is acting as the web server with the acutal content while the proxy container handles SSL and client side communications.  Also included is Terraform code that will spin up and build the demo in AWS.

Once built, the content can be accessed by hitting the host on standard HTTP or HTTPS ports.  If you access with HTTP, it will auto-redirect to HTTPS.

## Usage (local)
NOTE:  The following instructions assume that you have Docker and Docker Compose installed and available in your path.

* Clone this repo
* `cd tech_task/docker`
*  `docker build -t web`
*  `docker build -t proxy`
*  `docker-compose up -d`

## Usage (AWS)
NOTE:  The following instructions assume that you have Terraform v0.12+ installed and have your AWS CLI environment working.  This usually means a file at `~/.aws/credentials` with valid credentials in it.

* Clone this repo
* `cd tech_task/terraform`
* `terraform init`
* `terraform apply -auto-approve`
