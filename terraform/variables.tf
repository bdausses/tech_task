##################
# Global variables
##################
# Instance Key - The local copy of your key file.
variable "instance_key" {
  default = "~/.ssh/id_rsa"
}

###############
# AWS variables
###############
# AWS Region
variable "aws_region" {
  default = "us-east-2" # Ohio
}

# AWS Instance Type
variable "aws_instance_type" {
  default = "t3.medium"
}

# Key Name - The name of your key at AWS.
variable "key_name" {
  default = "bdausses_se"
}

# Provision AWS? - Switch used to control deployment to AWS
variable "provision_aws" {
  default = false
}
