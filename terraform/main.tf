# Set cloud provider
provider "aws" {
  region     = "${var.aws_region}"
  version    = "~> 2.29"
}

# Find the most recent CentOS 7 AMI
data "aws_ami" "centos" {
  most_recent = true

  filter {
    name   = "product-code"
    values = ["aw0evgkw8e5c1q413zgy5pjce"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["aws-marketplace"]
}

# Create Security Group
resource "aws_security_group" "bdausses-ttask-allow-all" {
  name        = "bdausses-ttask-allow-all"
  description = "Allow all inbound/outbound traffic"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

# Spin up the demo server
resource "aws_instance" "ttask_demo_server" {
  ami           = "${data.aws_ami.centos.id}"
  instance_type = "${var.aws_instance_type}"
  tags = {
    Name = "TTask_Demo_Server"
  }
  key_name      = "${var.key_name}"
  security_groups = ["${aws_security_group.bdausses-ttask-allow-all.name}"]
  root_block_device {
    delete_on_termination = true
  }
}

# Post-provisioning steps for demo server
resource "null_resource" "demo_preparation" {
  depends_on = ["aws_instance.ttask_demo_server"]

    connection {
      host        ="${aws_instance.ttask_demo_server.public_ip}"
      user           = "centos"
      agent = true
      #private_key    = "${file("${var.instance_key}")}"
      }

  # Install Docker, pull code, and start application
  provisioner "remote-exec" {
    inline = [
      "sudo yum install -y yum-utils device-mapper-persistent-data lvm2 git",
      "sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo",
      "sudo yum install -y docker-ce docker-ce-cli containerd.io",
      "sudo systemctl start docker",
      "sudo curl -L \"https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)\" -o /usr/local/bin/docker-compose",
      "sudo chmod +x /usr/local/bin/docker-compose",
      "sudo mkdir /opt/src",
      "cd /opt/src",
      "sudo git clone https://bdausses@bitbucket.org/bdausses/tech_task.git",
      "cd tech_task/docker",
      "sudo docker build -t web web",
      "sudo docker build -t proxy proxy",
      "sudo /usr/local/bin/docker-compose up -d"
    ]
  }
}
